You are here since you are interested for an internship opportunity with Khoslalabs. 

Please find below the list of assignments and choose your preferred assignment from the list.

Once you are chosen your project do mail us at internship@khoslalabs.com  with the  project tile of your choice and git user email id.

We will give you access to the Git Project for the assignment. 

Please refer the PDF document in the assignment project for more details of the problem and instructions on how to submit the solution.

Desired time period is 1 - 3 weeks.

For further queries and clarifications please reach us at internship@khoslalabs.com


List of  Assignments :
----------------------


1: face_capture_assignment_cross

    Create  a cross platform solution for quality face capture. By cross platform it means , solution should support Web Platform and Mobile Platform ( Android Preffered )
    
2: face_capture_assignment_web

    Create  a Web  solution for quality face capture. Web solution means it can be an Angular/React solution
     
3: face_capture_assignment_mobile

    Create  a Mobile  solution for quality face capture. Andorid App is preferred.
    
4: image_masking_assignment

    Solution should be capable to take an image having a text containing digits in sequence as input, and give us an output of the same image but a digits portion of the text masked.